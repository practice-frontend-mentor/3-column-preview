//Guardar estado columna activa
let columnaActiva=1;

//Seleccionar las columnas
const columnas=document.querySelectorAll(".article");

//escuchar los clicks
columnas.forEach((columna,indice) => {
    columna.addEventListener("click",function(){
        cambiarColumna(indice);
    })
});

function cambiarColumna(indice){
    columnas[columnaActiva].classList.remove("activa")
    columnas[indice].classList.add("activa")
    columnaActiva=indice;
}